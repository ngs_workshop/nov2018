# NGS data analysis on the Unix command line - DAY3

## mRNA-Seq ANALYSIS COOKBOOK

Before starting any analysis check your disk space and create a working directory.

### Preprocessing
Once you downloaded your data the first thing to do is to check the quality of your fastq files.

#### - Quality check

For this the most used tool is FastQC.

Create a directory where your results will be sent.

```sh
mkdir <name of directory>
ex : mkdir FastQC
```
Analyse the quality of your sequencing files
```sh
fastqc -o <output directory> --extract -f fastq <fastq file>
```
| Option     |      Meaning    |
| -------- | ------------ |
| -o    | output directory    |
| --extract      | decompress the output folder  |
| -f | input = fastq files |

#### - Read cleaning

If necessary according to the output of fastQC you will have to clean your data. This means the most of the time to remove reads from your dataset that have a bad quality (<30), remove the adapters, remove reads that are too short...

For this we use cutadapt.
```sh
# create output directory
mkdir Clean_Data
cutadapt -a <adapter_sequence> -A <adapter_sequence> -a "A{50}" -q 30 -m 10 -o
 <output_directory>/outfile1.fq -p <output_directory>/outfile2.fq <fastq_file>
```

| Option     |      Meaning    |
| -------- | ------------ |
|-a	|Adapter sequence|
|-A	|Adapter second pair (if PE)|
|-q	|Quality value|
|-m	|Minimum read length|
|-o	|Output directory|
|-p	|Output second pair (if PE)|

NB : A{50} is a regular expression that here looks for a stretch of A of length 50.

Think to verify the outputs of the trimming to verify that you have eliminated the sequences you don't want and also to check if you have not made a mistake in your command line.
This will also show you the number of clean reads you are going to use in the rest of the analysis.

-------------------------------------------

## MAPPING with HISAT 2

There are lots of mapping tools.
Some are specialized, some are more general. The choice of the tool will depend on the analysis you want to do and how you generated your sequences.

Here we want to study the differential expression of genes with a sequencing done Stranded and PE (paired end).

So we are going to use Hisat2.

#### - Creating Hisat2 index

Hisat2 needs an index to do the mapping.
You have to create it before doing the mapping. Hisat can also take into account information like splice sites and the position of the exons.
You can alos create an index without these information and add them afterwards during the mapping step.

Once the index is created you don't have to do it again.
In general you will have one index per species studied or an index for a specific organelle if the mapping on the complete genome doesn't interest you.
First of all create a directory for your index
```sh
mkdir Index
```

If you want to add information to your index you will have to launch the two scipts :
```sh
extract_splice_sites.py <gtf_file> > <output_directory>/gtf_id.ss
extract_exons.py <gtf_file> > <output_directory>/gtf_id.exon
```
They will create files with the coordinates of the splice sites and of the exons.

Then you create the index :
```sh
hisat2-build genome.fa index_dir/<nom_index>
Or with information
hisat2-build --ss gtf_id.ss --exon gtf_id.exon genome.fa index_dir/<nom_index>
```

NB: This step can take more or less time and needs more or less RAM. A simple hisat2 index of Arabidopsis takes less than 5 minutes on a desktop computer.
The creation of the Maize index with .ss and .exon files took around 5 hours on our computing cluster !!!

#### - Mapping
```sh
mkdir Mapping
hisat2 -t --rna-strandness RF --no-unal --summary-file <output_directory>/id.rf.out --new-summary
-x <index> -1 read_id_1.fastq -2 read_id_2.fastq --rg-id read_id --rg LB:PE150bp --rg PL:Sequencer --rg PU:Organism --rg SM:read_id
-S <output_directory>/id.rf.sam
```
| Option     |      Meaning    |
| -------- | ------------ |
|-t	|calculating time |
|--rna-strandness	| to specify the strandness|
|--no-unal| to not write unmapped reads in the .sam|
|--summary-file| writes the output in a file instead of stdout|
|-x| index |
|-1| reads R1|
|-2| reads R2 |
|--max-intronlen 2000 | to adapt considering the species you'r working on |
|--rg-id| info to add in .sam for downstream analysis|
|-S| name of .sam output|

Other option in SE :

| Option     |      Meaning    |
| -------- | ------------ |
|-U	| SE fastq file|

Once the mapping is finished you can check the results in the created summary file.
In general we expect a good mapping rate (> 90%) when we map wt reads on the corresponding reference genome.

--------
## COUNT READS
Before starting to count reads you have to prepare your files.
Hisat2 gives you .sam files that have to be converted to .bam files and sorted.
You have seen that bam files take much less space on your hard drive.

To prepare the files for counting we will use the samtools.
First we convert the .sam to .bam files :
```sh
samtools view -bS -o file.bam file.sam
```

Then we sort the file by name :

```sh
samtools sort -n -o file.sorted.bam file.bam
```

NB : you can also sort the file by position but you will have to specify it to htseq-counts.

NB : for visualization in IGV your files have to be sorted by position.

Then you can count the reads. For this we are going to use htseq-counts.
```sh
mkdir htseq
htseq-count -s reverse -f bam -m intersection-nonempty -r name -t gene -i ID bam.file gff.file > output-countGene.txt
```
| Option     |      Meaning    |
| -------- | ------------ |
|-s	|indicate strand |
|-f	|file format |
|-m| how to consider reads|
|-r| how your samfile was sorted (name or pos)|
|-t| type of feature you want to count |
|-i| feature ID name|


----------------------------------------------
