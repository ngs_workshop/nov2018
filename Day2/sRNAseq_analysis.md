# NGS and small RNA-seq fundamentals

### Workshop: NGS data analysis on the command line
### Day 2 - 26/11/2018

<br>

**Presentation by Sandrine Koechler**
>- Principles of NGS, devices
>   - Next-Generation Sequencing (NGS) technologies
>   - Illumina NGS: cluster formation, single-end vs. paired-end
>   - Library preparation: small RNA-seq vs. RNA-seq
>   - Visit to NGS facility (MiSeq), **Sandrine Koechler and Malek Alioua**

**Tutorial by Todd Blevins and David Pflieger**
>- Biological functions of small RNAs in plants
>- Review basic Unix commands from Day 1
>    - ls, cd, mkdir, mv, cp, tar, head, less, grep
>- small RNA-seq (Illumina data)
>    - raw FASTQ files
>    - Quality control, Phred scores (fastqc)
>    - small RNA-seq adapter trimming (cutadapt)
>- Read alignment
>    - Reference genomes (*A. thaliana*)
>    - Short read aligners (bowtie)
>    - SAM/BAM file processing (samtools)
>- Data analysis
>    - Genome browsing (JBrowse)
>    - Quantification/annotations (Shortstack)
>    - Data display (R, ggplot2)

## 1. Background

The last decade has seen a seismic shift in DNA sequencing capabilities: Illumina, Ion Torrent and SOLiD platforms generate tens of millions to billions of short reads per run, whereas PacBio and Nanopore systems can generate  contiguous reads of 40-500 kb. These next-generation sequencing (NGS) and long-read sequencing technologies each permit novel applications suited to their respective strengths and weaknesses. The following table shows sequencing technology attributes collated from [Mardis (2017) *Nature Protocols*](https://www.ncbi.nlm.nih.gov/pubmed/28055035), [Wikipedia](https://en.wikipedia.org/wiki/DNA_sequencing#High-throughput_methods) and other online sources:

##### Table 1. DNA sequencing technologies
| Technology        | Read length    | Reads per run   | Accuracy |
|:------------------|:---------------|:----------------|---------:|
| Illumina          | 50 to 300 bp   |  25 to 4000 M   | 99.9%    |
| Ion Torrent       | 200 to 400 bp  |  up to 80 M     | ~ 98%    |
| SOLiD             | 50 to 75 bp    |  1200 to 1400 M | 99.9%    |
| Nanopore          | up to 500 kb   |  50 to 200 K    | ~ 90%    |
| PacBio            | up to 40 kb    |  ~440 K         | ~ 87%    |
| Sanger            | 400 to 900 bp  |  N/A            | 99.9%    |

>To pursue your own NGS projects, you can contact the IBMP sequencing and/or the Bioinformatics service platforms. Specific information to help design your next NGS run is available here:
>- [Designing a next generation sequencing run ](https://genohub.com/next-generation-sequencing-guide/)
>- [Recommendations for sequencing coverage](https://genohub.com/recommended-sequencing-coverage-by-application/)
>
> To request a bioinformatic analysis at the IBMP, please fill in the following form at http://intranet.ibmp.unistra.fr/bioinformatique/ and  send it to ibmp-bioinfo@unistra.fr

#### 1.1. Let's get started!

**Small RNA-seq** -- i.e., the highly parallel sequencing of cDNA derived from small RNAs -- has become a routine technique for studying RNA silencing in model organisms, as well as in virology and certain clinical research settings. Projects aimed at understanding microRNAs, siRNAs, piwiRNAs and tRNA fragments (amongst other small RNA classes) thus depend on the precise mapping, annotation and quantification of NGS reads ([Axtell 2013 *Annu Rev Plant Biol*](https://www.ncbi.nlm.nih.gov/pubmed/23330790)).

During today's tutorial, we will look at **Illumina small RNA-seq data** from wild-type *A. thaliana*:
-  Col-0

and compare this to data from mutants affecting the RNA-directed DNA methylation (RdDM) pathway:
-  *nrpd1-3*
-  *nrpe1-11*

*NRPD1* and *NRPE1* encode the largest subunits of RNA polymerases IV and V, respectively. These specialized RNA polymerases facilitate the biogenesis of 24-nt siRNAs derived from transposable elements (TEs) in plants. Today's data are **raw FASTQ read files** from a recent Arabidopsis epigenomic study: [Yang et al. (2017) *Genome Biol.*](https://www.ncbi.nlm.nih.gov/pubmed/28569170)

#### 1.2. Prepare your analysis workspace

Create a new working directory. You may want to review the [command reference sheet](unix_linux_command_reference.pdf) from Day 1.

```sh
ls -l
cd ~/Desktop/IBMP_Bioinformatics_Workshop/DAY2
ls -l
mkdir sRNAseq_analysis
cd sRNAseq_analysis
```

Copy the raw libraries (fastq.gz) from the **sRNAseq_libraries** folder to your working directory **sRNAseq_libraries**.
⚠️ Always keep your raw data safe and back them up to prevent data loss!

```sh
ls -l ../sRNAseq_libraries/
cp ../sRNAseq_libraries/* .

# We can examine the content of a compressed file with zcat
zcat WT_sRNA.fastq.tar.gz | head -n 100
```

Extract all the sequencing files

```sh
# Decompress a tar.gz archive using several options of tar
tar -xvzf WT_sRNA.fastq.tar.gz
tar -xvzf nrpd1_sRNA.fastq.tar.gz
tar -xvzf nrpe1_sRNA.fastq.tar.gz
```

- How many lines represent each read?
- How many total reads are in the file?
- What is the length of the raw reads?
- Do you see evidence of 3' adapters?


#### 1.3. NGS data quality control with fastqc

We always perform basic quality control checks after receiving raw NGS data to assure that there are no technical problems or biases, which would affect the biological interpretation later on.

```sh
# Quality scores are encoded every 4th line
head WT_sRNA.fastq
tail WT_sRNA.fastq
less WT_sRNA.fastq
```

- How can we visualise FASTQ quality scores?

DNA sequencing quality is typically represented on a logarithmic scale from 0 to 40. These Q values are known as [Phred quality scores](https://en.wikipedia.org/wiki/FASTQ_format#Quality); they represent error probabilities at *each base call* in the sequencing read. In order to efficiently store and display the Phred score for each base position, [ASCII symbols](http://www.theasciicode.com.ar/) (5, 6, <, ?, @, D, etc.) are used as an "encoding" of the numerical score values:
```
Score encodings https://en.wikipedia.org/wiki/FASTQ_format#Encoding
===============

Encoding     !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHI
             |    |    |    |    |    |    |    |    |
Phred score  0....5...10...15...20...25...30...35...40
             |    |    |    |    |    |    |    |    |
Quality      worst................................best
```

The Phred score (Q) represents the probability \(P\) of any given base call being incorrect as follows:
>Q = -10 log<sub>10</sub>P

>P = 10<sup>-(Q/10)</sup>

But there is a much simpler way to remember these error probabilities \(P\):

##### Table 2. Interpreting Phred quality scores
| Phred score (Q)      |Error probability \(P\) |Accuracy |
|:---------------------|:-----------------------|--------:|
| 0                    |1 in 1                  |     0%  |
| 10                   |1 in 10                 |    90%  |
| 20                   |1 in 100                |    99%  |
| 30                   |1 in 1000               |  99.9%  |
| 40                   |1 in 10000              |  99.99% |


- Which of the following three Illumina reads is the best quality read?

```
@HISEQ2000:136:FC706VJ:2:3:1000:12850
ACACCCGTAAGGTAACAAACCGTTGGAATTCTCTAATAAAAATTAATAAA
+
CCCFFFFFHHHHDHHJJJJJJJIFFHHHFFFCC#&*)+1>7(1*)+1>7(
```
```
@HISEQ2000:136:FC706VJ:2:5:909:1115
ACACCCGTAAGGTAACAAACCGTTTGGAATTCTCGGGTGCCAAGGAACTC
+
CCCFFFFFHHHHDHHJJJJJJJIDHHJJDHHJJJJJJDHHJJJJJDH+,+
```
```
@HISEQ2000:136:FC706VJ:2:11:622:7726
ACACCCGTAAGGTAGTCATCGTTAAAATAATAAAAATTAAATTAGAGGAA
+
CCC#$FFFF%&'F#'5F%&>7(HHH'5>HHJ#$$#&*)+1C%&'5>7(1$
```

Nowadays we expect a Phred score superior to Q = 30 from Sanger, Illumina or SOLID sequencing runs. However, PacBio and Oxford Nanopore (long read) runs produce data with average Phred scores closer to Q = 10.

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) is the tool used by the majority of the bioinformatics community to evaluate raw Illumina sequencing data.

```sh
# Examine the command-line options and usage for fastqc
fastqc --help

# Evaluate the quality of the WT, nrpd1 and nrpe1 small RNA-seq read data
# FastQC generates a separate html report for each library
fastqc WT_sRNA.fastq nrpd1_sRNA.fastq nrpe1_sRNA.fastq

# MultiQC merges all the FastQC reports into an elegant, composite report
multiqc .

# We can open all these html documents as follows
firefox *.html
```

- What do the FastQC and MultiQC reports tell us about the data?

#### 1.4. Remove adapters with cutadapt

Generally, the 3' adapters used for small RNA-seq library construction are not  trimmed from raw NGS reads provided to you by sequencing companies and facilities. To bioinformatically trim this sequence from each cDNA insert, we need to identify the particular ligation adapter used for making our libraries. In our case, this was the standard Illumina RA3 adapter:

- RA3: 5'-TGGAATTCTCGGGTGCCAAGG-3'

**Note:** Current documentation for all Illumina kits and their adapters is available [here](https://support.illumina.com/content/dam/illumina-support/documents/documentation/chemistry_documentation/experiment-design/illumina-adapter-sequences-1000000002694-03.pdf). Even though some companies (like Fasteris or BGI) may also provide "clean data", you should always download the raw FASTQ data.

```sh
grep --color "TGGAATTCTCGGGTGC" WT_sRNA.fastq
grep --color "TGGAATTCTCGGGTGC.*" WT_sRNA.fastq
```

There are many different software options for trimming NGS reads:  https://omictools.com/adapter-trimming-category. However, our favorite tool is the python program called [cutadapt](http://cutadapt.readthedocs.io/en/stable/guide.html).

```sh
cutadapt --help
cutadapt -a TGGAATTCTCGGG --trim-n --minimum-length 15 --maximum-length 30 -q 30 --discard-untrimmed -o WT_sRNA.clean.fastq WT_sRNA.fastq
```

Try to **grep** the adapter sequence again in the cleaned .fastq files.

```sh
grep --color "TGGAATTCTCGGGTGC" WT_sRNA.clean.fastq
grep --color "TGGAATTCTCGGGTGC.*" WT_sRNA.clean.fastq
grep --color "N" WT_sRNA.clean.fastq

# Run the fastqc program again
fastqc WT_sRNA.clean.fastq
```

Our data look quite good now! We can proceed with the analysis. The next step will be to align our clean reads (xxx.sRNA.clean.fastq) to the *A. thaliana* reference genome.

## 2. Alignment to the genome

If you are working with a model organism like *Saccharomyces cerevisiae*, *Caenorhabditis elegans*, *Drosophila melanogaster* or *Arabidopsis thaliana*,
 these organisms' reference genomes were sequenced, assembled and annotated over many years by large research consortia.

#### 2.1. Alignment to the *A. thaliana* reference genome

*A. thaliana* is the most studied model plant in the world. The ecotype Col-0 was the first plant genome to be completely sequenced (Arabidopsis Genome Initiative *Nature* 2000). This reference assembly contains five nuclear chromosomes, as well as the two organelle genomes, and is available at https://www.araport.org/data/araport11, which is the new "Arabidopsis Information Portal" supplanting the legacy site https://www.arabidopsis.org/.

> **Reference genome** (or **reference assembly**): the raw DNA sequence, digitally stitched together (i.e., assembled) from smaller segments, to represent a species' complete set of genes in their correct order and context, along with as much intergenic and repetitive sequence data as possible.

> **Genome annotations**: the precise "start" and "stop" positions of experimentally determined (or predicted) genes and other sequence features. Annotations refer to coordinates in a reference assembly and thereby summarise past molecular genetic and biochemical insights that are extremely useful for comparison to novel -omics/NGS data.

Since 2000 the Arabidopsis Genome Initiative has released numerous versions of the *A. thaliana* Col-0 genome, pairing TAIR genome assemblies with synonymously versioned TAIR annotations. The last revision of genome coordinates occurred in the TAIR9 release (\*see table below). Concretely, this means that data from older publications (citing TAIR8 or earlier) must be converted to the current coordinates, which remained unchanged after TAIR9.

| Maintained by   | Release |
| --------------- | ------- |
| Araport11       | 2016    |
| TAIR10          | 2010    |
| TAIR9 (*)       | 2009    |
| TAIR8           | 2008    |
| TAIR7           | 2007    |
| TAIR6           | 2005    |
| ...             | ...     |
| TIGR1           | 2001    |
| *Nature paper*  | 2000    |


We cannot use the genome file available for download at https://www.arabidopsis.org/, because the chromosome names therein are not conventionally formatted. At the [Araport website](https://www.araport.org/) the names are as desired (Chr1, Chr2, Chr3, ... etc.), which saves us time. First, we need to log in. You can use the following guest credentials for now...

- username: araportguest1
- password: n%C66bnqQFGa

In your Firefox browser, navigate to https://www.araport.org/downloads/TAIR10_genome_release. Notice that next to the entry for **assembly/TAIR10_Chr.all.fasta.gz (34.30 MB)** there is a small hyperlink **>_** ; clicking on this link reveals a command that you can use to download the entire TAIR10 genome. The 'Authorization: Bearer 1546270a45e3ef997f55b353a1f48b' item, shown below, will differ in your case.

```sh
# Download the TAIR10 genome from the Araport website
# We are adding an option, --progress-bar, to monitor download progress
curl --progress-bar -O -H 'Authorization: Bearer 1546270a45e3ef997f55b353a1f48b' https://api.araport.org/files/v2/media/system/araport-public-files//TAIR10_genome_release/assembly/TAIR10_Chr.all.fasta.gz

# Alternatively, you can copy the genome file from our Back-up (BU) directory
cp -v /home/workshop-user/Desktop/IBMP_Bioinformatics_Workshop/DAY2/BU/ReferenceGenome/TAIR10_Chr.all.fasta.gz .
```
- How many chromosomes are shown in the file that you downloaded?
- How many individual DNA bases are recorded per chromosome?

```sh
# Extract a .gz archive with gunzip
gunzip TAIR10_Chr.all.fasta.gz
# Check the file
less TAIR10_Chr.all.fasta
# Check chromosomes
grep "^>" TAIR10_Chr.all.fasta
```

Instead of inspecting the individual chromosomes using **less** or another bash command, we can use a short python script called **seq_length.py** to print the length of each chromosome record in our *A.thaliana* genome FASTA file.
The script itself only contains nine lines of code: [seq_length.py](seq_length.py)

To use this python script, we type the following at the bash prompt:
```sh
python ../Scripts/seq_length.py TAIR10_Chr.all.fasta
```
You can check this output against expected values for the [reference assembly](https://www.arabidopsis.org/portals/genAnnotation/gene_structural_annotation/agicomplete.jsp). Note: seq_length.py calculates a genome size larger than the 5 chromosome "golden path", because we included organelle genomes.

#### 2.2. How to align the reads using bowtie

The [bowtie aligner](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) was the first implementation of the Burrows-Wheeler algorithm for short read alignment, and it has opened a new era in processing high-throughput data. There are two versions of bowtie. The latest version, bowtie2, is typically preferred because, as an algorithm, it is superior to bowtie1.

In general, all short read aligners operate on the same principles. They first build an index from the reference genome sequence, which only needs to be done once. The NGS reads in FASTA/FASTQ format are then aligned against this index.

```sh
# Look at the bowtie aligner's options
bowtie

# Build the reference genome index files for bowtie
bowtie-build TAIR10_Chr.all.fasta TAIR10_Chr.all
```

Alignment command for bowtie

```sh
# Align our WT library
bowtie TAIR10_Chr.all --sam --verbose -v 1 --best --strata -k 10 -q --al WT_sRNA.clean.mapped.fastq --un WT_sRNA.clean.unmapped.fastq WT_sRNA.clean.fastq WT_sRNA.clean.bowtie1.sam
```

- What do the following command-line options specify?
    - --sam
    - -v 1
    - -k 10
    - -q
    - --al \<filename\>
    - --un \<filename\>
- How many mapped reads and unmapped reads resulted from this command?

```sh
wc -l WT_sRNA.clean.mapped.fastq
wc -l WT_sRNA.clean.unmapped.fastq
```

#### 2.3. SAM/BAM formats

Various software tools, R libraries and Python modules can process SAM/BAM alignment files. However, bioinformaticians typically use [samtools](http://samtools.sourceforge.net/), because these compiled C utilities are optimized for common SAM/BAM viewing, sorting, filtering and interconversion tasks.

```sh
# Explore samtools' different sub-commands: view, sort, index ...
samtools --help
samtools view --help

# Look at some alignments with the "view" command
samtools view WT_sRNA.clean.bowtie1.sam | head -n 50
```

- How are the read alignments currently sorted in your SAM?
- Why do you think they are in this order, by default?
- What are the disadvantages of sorting alignments this way?

A BAM file (some_alignments.bam) is a compressed, binary representation of a SAM file (some_alignments.sam). This compression reduces the file size, and the binary representation makes processing by **samtools** more efficient, but BAM files are not human-readable in **less** or text editors.

We can use **samtools view** with the **-b** and **-S** options to read a SAM file and transform it into BAM, specifying an arbitrary filename after **-o**. Logically, we will choose the name "WT_sRNA.clean.bowtie1.bam" here.
```sh
samtools view -bS WT_sRNA.clean.bowtie1.sam -o WT_sRNA.clean.bowtie1.bam
```

- What is the size of your BAM file compared to the original SAM file?
- What happens if you try to open the BAM file using **less**?

Now we can efficiently sort alignments in the BAM file by chromosome position (**samtools sort**), again specifying the output filename after **-o**. The **samtools index** step allows other software (such as genome browsers) to load our alignment data using the resulting **.bai** index file.
```sh
# Sort your new BAM file
samtools sort WT_sRNA.clean.bowtie1.bam -o WT_sRNA.clean.bowtie1.sorted.bam
# Verify that the order of alignments has changed
samtools view WT_sRNA.clean.bowtie1.sorted.bam | head -n 50
# Index your BAM file
samtools index WT_sRNA.clean.bowtie1.sorted.bam
# Check that your .bai index was generated
ls -l
```

## 3. Small RNA annotation, quantification and data visualisation

#### 3.1. Using Shortstack to quantify small RNA-seq data

[**ShortStack**](https://github.com/MikeAxtell/ShortStack) is a software tool that facilitates the comprehensive annotation and quantification of small RNA reads based on the genomic distribution of their bowtie alignments to a reference genome. ShortStack detects clusters, analyzes the small RNAs' length distribution and can (optionally) predict miRNAs / pre-miRNAs using genomic sequence info from the putative  source loci (i.e., the TAIR10 reference assembly in our case). ShortStack also implements an algorithm to optimize the placement and counting of multi-mapping small RNA reads (see [Johnson et al. 2016 *G3*](https://www.ncbi.nlm.nih.gov/pubmed/27175019) for more details)

```sh
# Explore ShortStack's interface by calling it without arguments (options, filenames, etc.)
ShortStack
# For more details invoke the --help option
ShortStack --help
```

The simplest mode of ShortStack operation is to detect small RNA clusters using trimmed small RNA-seq read data (e.g., "WT_sRNA.clean.fastq" from above) and a suitable reference genome ("TAIR10_Chr.all.fasta").
To save time today, we have already generated the specialized version of bowtie alignments needed by ShortStack for small RNA functional annotation, and we have already converted these to sorted BAM files with corresponding indices (.bam and .bai). ShortStack can do all this with a single command, but it requires significant memory resources and time:

```sh
# Example command line for ShortStack
# **Do not run**: we have already run this part for you...
ShortStack --mismatches 0 --nohp --mmap u --pad 75 --mincov 5 --dicermin 15 --dicermax 30 --sort_mem 8G --bowtie_cores 2 --genomefile TAIR10_Chr.all.fasta --outdir WT_sRNA --readfile WT_sRNA.clean.fastq
```

- What do the following command-line options specify?
    - --mismatches 0
    - --nohp  
(Hint: look at the info displayed after typing "ShortStack --help" again).

You can find the resulting ShortStack alignment and analysis data as follows:

```sh
# ~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/BU/ShortStack contains pre-computed output for today's analyses
cp -vr ../BU/ShortStack .
cd ShortStack
ls -l
```

The idea is to quantify the number of small RNAs mapping to transposable elements (TE siRNAs) versus those mapping to *MIR* genes (microRNAs and other microRNA-related species). Using ShortStack's annotation functionality you can directly see the number of reads mapped by size class (e.g., 21, 22, 23 or 24 nt RNAs) at your specific locus of interest.

```sh
# Here are the loci that we want to quantify
less TE_miRNA.1000.annotation.shortstack.txt

# To quantify these now with ShortStack
ShortStack --nohp --locifile TE_miRNA.1000.annotation.shortstack.txt --bamfile WT_sRNA.clean.bam --genomefile TAIR10_Chr.all.fasta --outdir WT_sRNA_shortstack
```

#### 3.2 Visualization in a genome browser (JBrowse)

After indexing the sorted bam, we can now load it into a genome browser. Some such tools are "stand-alone", running directly on your workstation (e.g., [Integrative Genomics Viewer, IGV](http://software.broadinstitute.org/software/igv/)). However, other genome browsers integrate dozens or hundreds of datasets by following the client-server model (e.g., [JBrowse](https://apps.araport.org/jbrowse/?data=arabidopsis), [SALK](http://neomorph.salk.edu/1001_epigenomes.html))

Using Firefox, navigate to the JBrowse server at Araport:
- https://apps.araport.org/jbrowse/?data=arabidopsis

From the menu options *inside* this JBrowse interface, click **File** -> **Open track file or URL**

Load your small RNA-seq alignment files (.bam) with their indices (.bai) into JBrowse. Activate the "Repeat: Natural Transposons" annotation track using the sidebar at left. Now you can directly search for Transposable Element (TE) examples:

- AT3TE40125
- AT5TE25535
- AT2TE07550

Examine the distribution of siRNA clusters with respect to these TE annotations.

Still in JBrowse, find cytosine methylation data for WT Arabidopsis by looking in the "Tracks Available in Faceted List" (CoGE Plug-in, sidebar).

- What type(s) of cytosine methylation (CG, CHG or CHH) seem to correlate best with clusters of NRPD1-dependent siRNAs?

#### 3.3. R analysis: size distributions, functional annotation

We are going to analyze ShortStack results using [RStudio](https://www.rstudio.com/).
You can open the tutorial called **ShortStack_analysis.html** in the **DAY2** folder and then start Rstudio.




#### 3.4. Using ShortStack for the *de novo* detection of microRNAs

When run on a workstation or cluster with enough memory, ShortStack can readily detect unannotated miRNA/miRNA\* sequences *de novo* based on the predicted secondary structures of their putative RNA precursors.

```sh
# Example command line for de novo detection of miRNAs. NOTE: --nohp is omitted to switch on miRNA prediction
# **Do not run**: we have already run this part for you...
ShortStack --mismatches 0 --mmap u --bowtie_m 50 --bowtie_cores 4 --pad 75 --mincov 5 --dicermin 20 --dicermax 24 --foldsize 300 \
--sort_mem 8G --genomefile TAIR10_Chr.all.fasta --outdir WT_sRNA_miRNAsearch --readfile WT_sRNA.clean.fastq
```



```sh
# Let's examine ShortStack's output for the de novo detection of miRNAs in our WT sample
cd ~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/BU/ShortStack_denovo/WT_sRNA_miRNAsearch
ls -l
```

```sh
# ShortStack's Log file summarizes bowtie mapping and miRNA detection stats, amongst other valuable info
less Log.txt

... bowtie stats ...
ven. févr. 23 13:57:40 CET 2018
Starting alignment of WT_sRNA.clean.fastq with bowtie command:
        bowtie -q -v 0 -p 4 -S -a -m 50 --sam-RG ID:WT_sRNA.clean ./TAIR10_Chr.all - < WT_sRNA.clean.fastq
        Completed. Results are in temporary file WT_sRNA_miRNAsearch/WT_sRNA.clean_readsorted.sam.gz pending final processing
        Unique mappers: 995411 / 6374047 (15.6 %)
        Multi mappers: 4743421 / 6374047 (74.4 %)
        Multi mappers ignored and marked as unmapped: 77609 / 6374047 (1.2 %)
        Non mappers: 557606 / 6374047 (8.7 %)

... miRNA stats ...
ven. févr. 23 14:15:05 CET 2018
Tally of loci by predominant RNA size (DicerCall):

DicerCall	NotMIRNA    MIRNA
N or NA		3825    		0
20      	59      		5
21      	247     		29
22      	277     		3
23      	955     		0
24      	17742   		0
```

```sh
# In the MIRNAs directory are individual analysis files for each putative miRNA locus
cd MIRNAs
ls -l
# We can concatinate all the putative miRNA loci into one file and explore it using *less*
cat Cluster* > WT_miRNA_clusters.txt
less WT_miRNA_clusters.txt
```

#### 3.5. R visualization of ShortStack's *de novo* microRNA analysis

Now we will continue analyzing ShortStack's *de novo* results using [RStudio](https://www.rstudio.com/):
return to **ShortStack_analysis.html** in **DAY2**.

## 4. Concluding remarks:  NGS analysis in the Unix ecosystem

Today we have shown you a rudimentary small RNA-seq analysis as an example workflow for NGS analysis. **bowtie**, **samtools** and **ShortStack** have many command-line options, making these tools amazingly flexible, especially when combined with quantitative analyses and display in **RStudio**.

Countless bioinformatic tools have been developed since the advent of DNA sequencing. Many were used for a time but then abandoned because of poor maintenance, compatibility issues or costly licenses. By contrast, free packages like **bowtie** and **samtools** (similar to [**BLAST**](https://www.ncbi.nlm.nih.gov/pubmed/2231712) in the 1990s), have become core tools for NGS work, only occasionally supplanted by equivalent software with improved performance. Key to this success is the tools' adherence to the [design philosophy of UNIX-like operating systems](https://en.wikipedia.org/wiki/Unix_philosophy):

- Write programs that do one thing and do it well.
- Write programs to work together.
- Write programs to handle text streams, because that is a universal interface.

As we develop NGS analysis pipelines to address specific research questions, we can also benefit from applying this "UNIX philosophy". If our analyses are coded in well-documented scripts (**bash**, **R** or **python**), processing standard file formats (**Appendix I**) via open-source tools (e.g., **Appendix II**), the longterm reproducibility of the work will be guaranteed. Furthermore, the ability to share and reuse our bioinformatic scripts/pipelines saves time and other precious resources.


### Appendix I.  Bioinformatic file formats

Modified from [Wikipedia](https://en.wikipedia.org/wiki/List_of_file_formats#Biology), [UCSC](https://genome.ucsc.edu/FAQ/FAQformat.html) and [COMAV](https://bioinf.comav.upv.es/courses/sequence_analysis/sequence_file_formats.html):
- [**AB1**](https://seqcore.brcf.med.umich.edu/content/i-downloaded-my-chromatogram-file-now-how-do-i-open-it) – Chromatogram files used by instruments from Applied Biosystems for Sanger sequencing output.
- [**BAM**](https://en.wikipedia.org/wiki/SAM_(file_format)) – The "Binary Alignment/Map" format (compressed SAM format)
- [**BED**](https://genome.ucsc.edu/FAQ/FAQformat.html#format1) – This "Browser Extensible Display" format can be used for describing genes and other features of DNA sequences.
- [**FASTA**](https://en.wikipedia.org/wiki/FASTA_format) – The simplest form of sequence format, FASTA uses ">" to indicate a solitary header above each sequence. FASTA files do not contain internal feature annotations or quality scores.
- [**FASTQ**](https://en.wikipedia.org/wiki/FASTQ_format) – FASTQ files contain sequence data with corresponding Phred quality scores; Illumina FASTQ output has headers that specify the flow-cell ID, lane, cluster position and multiplexing barcode of each read.
- [**GenBank**](https://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html) – The file format used by the U.S. National Center for Biotechnology Information (NCBI) to represent database records for nucleotide (**.gb**) and peptide (**.gp**) sequences. GenBank files typically indicate internal feature annotations, references and and other metadata.
- [**GFF**](https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md) – The "General Feature Format" is used to describe genes and other features of DNA, RNA, and protein sequences.
- [**SAM**](https://en.wikipedia.org/wiki/SAM_(file_format)) – The "Sequence Alignment/Map" format was originally used to release results from the 1000 Genomes Project. Now, almost all genomic projects process alignment data via SAM or its compressed binary equivalent (**BAM**).
- [**SRA**](https://www.ncbi.nlm.nih.gov/sra) – The format used by the NCBI Short Read Archive to store high-throughput DNA sequence data.
- [**TXT**](https://en.wikipedia.org/wiki/Tab-separated_values) - Not all data are stored in standard bioinformatic formats. Data tables often reside in application-specific text files. For example, ShortStack generates a "Results.txt" file, which contains Tab-Separated Values (**TSV** format). Another common text format uses commas to separate values (Comma-Separated Values, **CSV** format).
- [**VCF**](http://www.internationalgenome.org/wiki/Analysis/vcf4.0/) – Variant Call Format, a standard created by the 1000 Genomes Project in order to list and annotate the entire collection of human variants.


### Appendix II.  NGS software tools and languages

- [**bash** / **shell**](https://www.gnu.org/software/bash/) – In Mac OS X and Linux operating systems (including your CentOS virtual machine), the default interactive "shell" used for command line input and output is called bash. Scripts written in the shell language have the file extension **.sh**; bash accepts a particular dialect of shell scripts, and its functionality depends on what software tools are installed.
- [**bowtie**](http://bowtie-bio.sourceforge.net/index.shtml) – This C program implements the Burrows-Wheeler algorithm for ultrafast NGS read alignment. Bowtie divides the task across multiple processors and outputs alignments in  SAM format, allowing it to interoperate with tools supporting SAM (samtools, SNP callers, ShortStack etc.). For mRNA-seq applications other aligners are  used: [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) or [hisat2](https://ccb.jhu.edu/software/hisat2/index.shtml).
- [**cutadapt**](http://cutadapt.readthedocs.io/en/stable/guide.html) – This Python program performs a series of preprocessing steps on raw NGS reads: cutadapt will find and remove adapters, primers, polyA tails or other unwanted sequences, all depending on the options and information your provide on the command line.
- [**fastqc**](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/bad_sequence_fastqc.html) – This JAVA program performs NGS quality control on FASTQ data to generate graphical HTML reports (one per sample).
- [**IGV**](http://software.broadinstitute.org/software/igv/) – The "Integrative Genomics Viewer" is a stand-alone tool for the interactive exploration of genomic datasets (i.e., a genome browser). It supports a wide variety of data types and genomic annotations.
- [**JBrowse**](https://apps.araport.org/jbrowse/?data=arabidopsis) – An embeddable genome browser built with JavaScript and HTML5 (i.e., using the Client-Server model). JBrowse can be configured to display data from GFF3, BED, FASTA, Wiggle, BigWig, BAM, VCF and other standard file formats. Additional flexibility is afforded by browser [Plug-ins](https://gmod.github.io/jbrowse-registry/).
- [**multiqc**](http://multiqc.info/) – This Python program aggregates results from multiple fastqc runs to compare NGS quality control analyses across many samples into a single HTML report.
- [**perl**](https://www.perl.org/) – This venerable scripting language is used by several NGS analysis programs, notably **shortstack**, to execute and coordinate the input/output of core NGS tools and then format data tables.
- [**python**](https://www.python.org/) – A high-level language for general-purpose programming created by Guido van Rossumm, which is now widely used for scientific computing.
- [**R**](https://www.r-project.org/about.html) – A programming language for statistical computing and graphics, which is often used downstream of NGS read alignment and **samtools** to perform quantitative analyses and data display. [**RStudio**](https://www.rstudio.com/products/rstudio/#Desktop) is freely downloadable integrated development environment for R that includes a console, a text editor with code execution, as well as tools for plotting, history, debugging and workspace management ([R command summaries](https://www.rstudio.com/resources/cheatsheets/)).
- [**samtools**](https://en.wikipedia.org/wiki/SAMtools) – A powerful set of C programs (samtools **view**, samtools **sort**, samtools **index**, etc.) used for post-processing and interconverting NGS read alignment data in the **SAM** (Sequence Alignment/Map) and **BAM** (Binary Alignment/Map) formats.
- [**shortstack**](http://sites.psu.edu/axtell/software/shortstack/) – This Perl program coordinates the analysis of small RNA-seq data using core NGS bioinformatics tools (**RNALfold**, **samtools**, **bowtie**, etc.) in order to determine the genomic position, biogenesis mode and function of small regulatory RNAs.

### Appendix III.  References + Data sources

- Mardis (2017). DNA sequencing technologies: 2006-2016. *Nat Protoc.* **12**(2):213-218.
- Axtell (2013). Classification and comparison of small RNAs from plants. *Annu Rev Plant Biol.* **64**:137-59.
- Yang et al. (2017). The developmental regulator PKL is required to maintain correct DNA methylation patterns at RNA-directed DNA methylation loci. *Genome Biol.* **18**(1): 103.
- Shahid and Axtell (2014). Identification and annotation of small RNA genes using ShortStack. *Methods* **67**(1):20-7
- Johnson et al. (2016). Improved Placement of Multi-mapping Small RNAs. *G3* **6**(7):2103-11.
- Illumina kits and adapters documentation:
https://support.illumina.com/content/dam/illumina-support/documents/documentation/chemistry_documentation/experiment-design/illumina-adapter-sequences-1000000002694-03.pdf
- Download site for TAIR10 *Arabidopsis thaliana* reference genome: https://www.araport.org/downloads/TAIR10_genome_release
