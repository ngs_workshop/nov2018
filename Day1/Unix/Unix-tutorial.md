# UNIX FOR BIOLOGISTS

## Valérie Cognat
## Platform B2I / Bioinformatics - IBMP

##     
<br />

## PROGRAM

  * Why unix ?

  * The terminal window

  * The Unix file system

  * Working with files and directories

  * Manipulating text files

  * File transfer between computer / server


## INTRODUCTION


### Why UNIX ?

  * Majority of bioinformatics software developed only for Unix

  * Most programs are command-line

  * Graphical and/or web user lack of flexibility needed in research


### What is UNIX ?

  * Operating System developed since 1969

  * Stable

  * Multi-user (login/pwd)

  * Multi-tasks

  * Different unix system : Linux / MacOs


### What is a virtual machine (VM) ?

  * Emulation of an operating system

  * VirtualBox

  * Free and open source

  * Can be install on several OS (MacOs, Linux, Windows)

  * to create and manage VM

  * Your VM :

  * Linux CentOS  7

#### => Open it & connection


## THE TERMINAL & COMMAND LINES


### UNIX

![unixArchi](img/unix-archi.png)


### The terminal & command lines

  * Console through which you could interact with a computer

  * Shell is the command line interpreter (bash)

  * Command line : instruction in one line and Enter

  * Commands deal with files and processes

  * Request information (list files in directory)

  * Launch a simple task (rename file)

  * Start / stop bioinformatics tool


### Some useful command lines

  * List directory contents
  ```
  $ ls
  $ ls -la
  ```

  * System information
  ```
  $ uname -a
  ```

  * Print the name of the current directory
  ```
  $ pwd
  ```

  * Who is logged on
```
$ who
```

### Access to manual

```
  $ man <command>
```

  * Options for « ls »
```
  $ man ls
```

  * Show  all files (with masked files)
  ```
  $ ls – la
  ```

  * Show human readable size
  ```
  $ ls – lh
  ```

  * Sorted according modification time
```
$ ls – lt
$ ls -ltr
```

### Standard output and Redirection

  * STDOUT / STDERR

  * Redirection to a file
  ```
  $ who > OUT.log 2> ERR.log
  ```

  * Look at files
  ```
  $ less OUT.log
  $ cat OUT.log
  $ more OUT.log
  $ tail OUT.log
  $ zcat file.gz
  ```


### Open a file in a text editor

  * nano
  ```
  $ nano OUT.log
  ```

   * Ctrl tab + O to register

   * Ctrl tab + X to quit

  * vi/vim
  ```
  $ vi OUT.log
  $ vim OUT.log
  ```

   * i to insert something

   * :q! to quit

   * :wq! to write and quit

### Trip and tricks

  * Tab key for autocompletion

  * Up/down array keys to retrieve command lines

  * History
```
$ history
```

## THE UNIX FILE SYSTEM


### The file system hierarchy (FSH)

![fsh](img/fsh1.png)

### The PATH

![fsh](img/fsh2.png)


### UNIX directory tree

  * Path : unique location of a file/directory

  * Absolute path : location of a file/dir from the root directory

  * Relative path : location of a file/dir related to the current working dir

  * What is my current dir ?
  ```
  $ pwd
  ```
  * Home : user’s directory

   * /home/workshop-user

   * Shortcut to homedir : ~



### Move into Unix directory tree

  * cd : Change directory
  ```
  $ pwd
  $ ls
  $ cd Desktop/IBMP_Bioinformatics_Workshop
  $ ls
  $ cd DAY1
  $ ls -l
  $ cd ./
  $ pwd
  $ cd ../..
  $ pwd
  $ cd ~
  ```
  * Some environment variables
```
$ echo $USER
$ echo $HOME
$ echo $PATH
```

## FILES AND DIRECTORIES


### The files

  * Basic files

  * text

  * data (fasta, fastq, gff)

  * script files (shell, bash, pl, py, ...)

  * Binary files

  * Not human readable

  * Executables (tools : samtools, bwa)

  * Data : Bam files

  * Compressed : .gz, .zip ...


### Basic manipulation of files

  * Create a text file
  ```
  $ cd ~/TP_UNIX
  $ touch myfile.txt
  ```
  * Open the file
  ```
  $ nano myfile.txt
  $ cat myfile.txt
  ```
  * Rename the file
  ```
  $ mv myfile.txt mynewfile.txt
  ```
  * Copy the file
  ```
  $ cp mynewfile.txt myfilecp.txt
  ```
  * Move the file in Documents dir
  ```
  $ mv myfilecp.txt Documents/
  $ ls Documents/
  ```
  * Remove the file
  ```
  $ rm myfilecp.txt
  ```

### The directories

  * Create a directory
  ```
  $ cd ~
  $ mkdir mydir
  ```
  * Copy a directory
  ```
  $ cp – r mydir mydircp
  ```
  * Remove a directory
  ```
  $ rm – r mydircp
  ```
  * Rename a directory
  ```
  $ mv mydir MYNEWDIR
  ```
  * Go in / Go out
  ```
  $ cd MYNEWDIR
  $ cd ..
  ```
  * Listing content of a directory
  ```
  $ ls MYNEWDIR
  ```

### Some useful commands

  * file: determine file type
  ```
  $ cd ~/TP_UNIX
  $ file myfile.txt
  ```
  * find: find a file
  ```
  $ man find
  $ find ~ –name nrpd1_sRNA.clean.fastq –print
  $ find ~ –name nrpd1_sRNA.clean.fastq –ls
  ```

### Exercice

  * Create a TP_UNIX directory in your home

  * Verify it is creating

  * Find the path of WT_sRNA.clean.fastq

  * Copy the file in your TP_UNIX dir

  * Rename the file in lib1.fastq

  * Verify


### Answer
```
$ cd ~

$ mkdir TP_UNIX

$ cd TP_UNIX

$ ls

$ cd TP_UNIX

$ find ~ -name WT_sRNA.clean.fastq

$ cp ~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/BU/sRNAseq_analysis/WT_sRNA.clean.fastq lib1.fastq

$ ls -lh
```

### Best practices

  * Names are case sensitive !

  * No space in names : lesers, numbers, dot, underscore, ....

  * Avoid special characters : $@&^

  * Extensions are not necessary but give information for the type of file


## MANIPULATION OF FILES


### Compress / uncompress files

  * zip (.zip)
  ```
  $ zip lib1.fastq.zip lib1.fastq
  $ unzip mylib.zip
  ```
  * bzip2 (.bz2)   
     *<-k> to keep original file*
     ```
     $ bzip2 lib1.fastq
     $ bzip2 –dk lib1.fastq.bz2
     ```
  * gzip (.gz)  
     *<-k> to keep original file*
  ```
  $ gzip lib1.fastq
  $ gunzip lib1.fastq.gz
  ```
  * tar (.tar / .tar.gz)
```
$ tar -czf lib.tar.gz lib1.fastq
$ tar –xvf lib.tar.gz
$ tar –tvf lib.tar.gz
```

### Exercice

  * Create dir

  * Copy the dir sRNAseq_libraries/ in the new dir

  * Copy TAIR10_Chr.all.fasta.gz in the new dir

  * Go into the new dir

  * Untar all data

  * Look at the files


### Answer
```
$ cd ~
$ mkdir TP2
$ cd TP2
$ cp -r ~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/sRNAseq_libraries.
$ cp ~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/BU/ReferenceGenome/TAIR10_Chr.all.fasta.gz.
$ gunzip TAIR10_Chr.all.fasta.gz
$ cd sRNAseq_libraries
$ file WT_sRNA.fastq.tar.gz
$ tar –xvf WT_sRNA.fastq.tar.gz
$ tar –xvf nrpd1_sRNA.fastq.tar.gz
$ tar –xvf nrpe1_sRNA.fastq.tar.gz
$ cat WT_sRNA.fastq
$ file WT_sRNA.fastq
```

### Text file manipulations

#### Open file
  ```
  $ more WT_sRNA.fastq
  $ cat WT_sRNA.fastq
  $ less WT_sRNA.fastq
  $ tail WT_sRNA.fastq
  ```
  * **more** : Page per page view
  * **cat** : concatenate and read file
   - Useful to concatenate multifiles
   - Reads files sequentially  
  * **less** : similar to more
   - allows backward movement in the file as well as forward movement
   - Faster with large  input  files  
  * **head** : display the first part of the file
  * **tail** : display the last part of the file


#### Extract a part of a file

  * Print the first 400 lines of a sequence file
  ```
  $ head -n 400 lib1.fastq
  ```
  * Print from line 1000 to the end of a file
  ```
  $ tail -n 1000 lib1.fastq
  ```
  * Print the last 100 lines of a file
  ```
  $ tail -n -100 lib1.fastq
  ```
  * Extract line 100 to 200 : use the | symbol ( pipe )
  ```
  $ tail -n 100 lib1.fastq | head – n 100
  ```
  * Save the output to a new file
```
$ tail -n 100 lib1.fastq | head – n 100 > lib1-100-200.fastq
```

#### Search keyword in file

  * Grep
  ```
  $ grep – help
  ```
  * Look at the sequence names in the genome fasta file
  ```
  $ grep ">" TAIR10_Chr.all.fasta
  ```
  * Search adapter in read fastq file
  ```
  $ grep --color "TGGAATTCTCGGGTGC" WT_sRNA.fastq
  ```
  * Find a specific read with its quality (-A / -B)
  ```
  $ grep -A 3 "@HWI-ST1435:229:HG5LYBCXX:1:1101:6259:2123" <  WT_sRNA.fastq
  ```
  * Other options :
   - Ignore case : -i
   - Exclude pattern : -v
   - Only whole words: -w


#### Other text file utilities

  * wc : count lines, words, characters
  ```
  $ cp ~/Desktop/IBMP_Bioinformatics_Workshop/DAY3/
  Araport11_GFF3_genes_transposons.201606.gff.
  $ wc – l Araport11_GFF3_genes_transposons.201606.gff
  $ wc – w Araport11_GFF3_genes_transposons.201606.gff
  $ wc – m Araport11_GFF3_genes_transposons.201606.gff
  ```
  * cut : select columns
  ```
  $ cut -s -f9 Araport11_GFF3_genes_transposons.201606.gff
  ```
  * sort : sort a file over columns
   - k : column number
   - n : numerically (default : alphabetically)
   - R : reverse
```
$ sort -k1,1 -k4,4n -k5,5n < myfile.gff
```
  * uniq : filter repeated lines
  ```
  $ grep "^Chr" Araport11_GFF3_genes_transposons.201606.gff | cut -f1 | uniq
  $ grep "^Chr" Araport11_GFF3_genes_transposons.201606.gff | cut -f1 | sort –u
  ```
  * paste : append columns to a file ( column order)
  ```
  $ paste libname.txt condition.txt
  ```
  * column – t : format output in columns
```
$ paste libname.txt condition.txt | column -t
```
   !!!! not working with too large files !!!!
```
$ head Araport11_GFF3_genes_transposons.201606.gff | column -t
```

### Exercice

  * Copy the Araport gff file in you working directory (DAY3)

  * File describing gene annotations for Arabidopsis genome

  * TAB-delimited with following columns:
    - Chromosome
    - Source
    - Feature
    - Start position
    - End position
    - Score
    - Strand
    - Frame
    - Attribute

  * Extract only gene features with columns 9-1-4-5 (same order)

  * Sort file by chromosome and end position


### Answer
```
$ cd ~/TP_UNIX
$ cp ~/Desktop/IBMP_Bioinformatics_Workshop/DAY3/Araport11_GFF3_genes_transposons.201606.gff.
$ grep -w "gene" < Araport11_GFF3_genes_transposons.201606.gff >genes.gff
$ cut -s -f9 genes.gff > gene_attr
$ cut -s -f1,4,5 genes.gff > gene_coord
$ paste gene_attr gene_coord | sort -k2,2 -k4,4n > genes-coord-sorted.txt
$ head -n 100 genes-coord-sorted.txt
```

## FILE PERMISSIONS


### Owner & group

![perm](img/file-perm.png)
  - d: directory (or - if file);
  - r: read permission;
  - w: write permission;
  - x: execute permission (or permission to « cd » if it is a directory);
  - -: no permission


### Change file permissions

#### Change file mode

  * chmod : change file mode
  ```
  $ chmod o-rwx /home/workshop-user
  ```
  * Make home directory inaccessible to others (o)
  ```
  $ chmod o-rwx /home/workshop-user
  ```
  * Make script executable by owner (u) and group (g)
  ```
  $ chmod ug+x myscript.sh
  ```
  * Make file readable by owner (u) and group (g)
  ```
  $ chmod ug+r myscript.sh
  ```

  * Use numbers
  ```  
  rwx r-x r—
  111 101 100 (binary code)
  ```

| bin   | 000 | 001 | 010 | 011 | 100 | 101 | 110 | 111 |
|---|---:|---:|---:|---:|---:|---:|---:|---:|  
| **digit** | **0** | **1** | **2** | **3** | **4** | **5** | **6** | 7** |

  ```
  $ chmod 754 myfile
  ```


#### Change file owner

  * chown: change file owner
  ```
  $ chown user:group <file/dir>
  ```
   * user can be username ou uid
   * group can be groupname ou gid

  * chmod/chown : change recursively in dir   
  ```
  $ chown – R user:group <dir>
  ```
```
$ chmod –R o-rwx /users/ibmp
```

## TO GO FURTHER


### AWK / SED

  * Awk (Aho, Weinberger, Kernighan)
   - Programming langage for text processing
   - Very useful for text extraction

  * sed (Stream EDitor)
   - Support regular expression
   - Replace string in file, change filenames, supress blank lines ....

  * Example : extract read length from a fastq file

```
$ head -n 10000 lib1.fastq > lib1-10000.fastq
$ awk '{if(NR%4==2) print length($1)}' lib1-1000.fastq | sort | uniq -c
```
```
$ grep -w "gene" Araport11_GFF3_genes_transposons.201606.gff | awk '{print $9"\t"$1"\t"$4"\t"$5}'
```
```
$ sed '/^$/d' file.txt
```

### Download data from web or server

![dwld](img/dwld.png)


### File transfer

  * From Web : wget

  * From a server (e.g. IBMP Server) : scp

  * What is md5sum ?

  * Calculate and check downloaded data

```
$ wget -O GSE48191.tar \
'http://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE48191&format=file’
$ wget -r https://data.fasteris.com/private/CLIENT_CODE/PATH_TO_FOLDER \
--no-parent --user=YOUR_USERID --password=YOUR_PASSWORD -A fastq.gz \
--no-check-certificate
```
```
$ md5sum *
```
```
$ scp <file> login@babel.ibmp.unistra.fr:
$ scp – r <dir> login@babel.ibmp.unistra.fr:
```

### Modify bash profile files

  * Bash shell startup files : bashrc, bash_profile, ....

  * Bashrc :

  * /etc/bashrc for all users

  * .bashrc in your home : personal alias and functions

```
$ ls –la
$ nano .bashrc
```
```
alias ls='ls --color=auto'
alias grep='grep --color=auto’
export PATH="/home/workshop-user/anaconda2/bin:$PATH"
```
```
$ source .bashrc
```

### Running applications

  * Find a tool : which
  ```
  $ which samtools
  ```
  * Run a tool with absolute or relative path
  ```
  $ /home/workshop-user/anaconda2/bin/samtools
  $ ~/anaconda2/bin/samtools
  $ samtools
  ```
  * Have a look to the processus
  ```
  $ top
  $ ps -ef | grep "workshop-user"
  ```
  * Stop application (ctrl c)

  * Kill application
```
$ kill <pid>
$ kill -9 <pid>
```

## CONCLUSION


### CONCLUSION

  * Unix is a powerful environment for biological data manipulation

  * More flexible than web interface


### TUTORIAL

  *  [command line tutorial](http://korflab.ucdavis.edu/bootcamp.pdf)
