# Workshop: NGS data analysis on the command line

**IBMP, Strasbourg - 22, 23, 26 & 27 November 2018**

### Overview

During this 4-day workshop we will introduce you to the Linux environment, to shell commands and to basic R scripting. Using these **command-line skills (Day 0 & Day1)**, we will then embark on two NGS data analyses -- **small RNA-seq (Day 2)** and **RNA-seq (Day 3)** -- based on published datasets from the organism *Arabidopsis thaliana*.

### Instructors

* (Au)de Gerbaud
* (Da)vid Pflieger
* (Hé)lène Zuber
* (Sa)ndrine Koechler
* (Ma)lek Alioua
* (St)éfanie Graindorge
* (To)dd Blevins
* (Va)lérie Cognat

## Locations

All trainings will be held in room 243.

## Requirements

A laptop with at least 4Go RAM and 80Go free space on the hard drive.
[VirtualBox](https://www.virtualbox.org/) in order to load the CentOS7 Virtual Machine.

## Schedule

⚠️ Lunch is not provided but there will be free coffee available

### Day 0 (Thursday, November 22th) - Unix for biologists

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 14:00 | **Welcome and Introduction to Unix environment** | Va + Au + St |
| 14:15 | **Introduction to command line (bash)** | Va + Au + St |
| 16:00 | Afternoon Break | |
| 18:00 | Close Day 0 | |

### Day 1 (Friday, November 23th) - Introduction to R

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 10:30 | **Introduction to R: Core Language Tutorial** | Da + Va + Au |
| 12:30 | Lunch, *on your own* | |
| 13:30 | **Data processing and data visualization** | Da + Va + Au |
| 15:00 | Afternoon Break | |
| 15:30 | **Statistical tests in R** | Da + Va + Au |
| 17:00 | Close Day 1 | |

### Day 2 (Monday, November 26th) - NGS and small RNA-seq fundamentals

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Introduction to Next-Generation Sequencing (NGS) technologies** | Sa + Ma |
| 10:30 | Morning Break | |
| 11:00 | **small RNA-seq data quality control & trimming** | Da + To + Au |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **small RNA-seq alignment to reference genome** | Da + To + Au |
| 15:00 | Afternoon Break | |
| 15:30 | **small RNA-seq visualization and analysis (JBrowse, R and ggplot2)**| Da + To + Au |
| 17h30 | Close Day 2 | |

### Day 3 (Tuesday, November 27th) - RNA-seq analysis

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Introduction to RNA-seq data acquisition and preprocessing of RNA-seq reads** | Hé + St + Au |
| 10:30 | Morning Break | |
| 11:00 | **Mapping of RNA-seq data and preparing files for Differential Expression analysis** | Hé + St + Au |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **Analysis of *A.thaliana* RNA-seq data** | Hé + St + Au|
| 15:30 | Afternoon Break | |
| 16:00 | **IGV and visualization tools** | Hé + St + Au |
| 17:30 | Close Day 3 | |
